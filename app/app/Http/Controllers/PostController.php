<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Models\Post;
use App\Notifications\MailNotification;
use App\Notifications\TelegramNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class PostController extends Controller
{
    private Post $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function create(CreatePostRequest $request)
    {
        $this->post->user_id = Auth::id();
        $this->post->title = $request->title;

        $to_name = $request->user()->name;
        $to_email = $request->user()->email;
        $data = array(
            'name' => "Ogbonna Vitalis(sender_name)",
            "body" => "A test mail"
        );

        try {
            Mail::send('dashboard', $data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('Laravel Test Mail');
            });
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }

        $this->post->notify(new TelegramNotification($request->user()->name));

        $this->post->save();

        return redirect()->route('posts')->with('message', 'Пост успешно сохранён');
    }
}
