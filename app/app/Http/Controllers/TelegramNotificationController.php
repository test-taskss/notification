<?php

namespace App\Http\Controllers;

use App\Notifications\TelegramNotification;
use Illuminate\Http\Request;

class TelegramNotificationController extends Controller
{
    public function send(Request $request)
    {
        $user = auth()->user();
        $user->notify(new TelegramNotification($request->notification));

        return back();

    }
}
