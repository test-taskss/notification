<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Channels\MailChannel;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

final class MailNotification extends Notification
{
    use Queueable;

    protected array $data;

    public function __construct($data)
    {
        $this->data = $data;
//        dd();
    }

    public function via($notifiable): array
    {
        return [MailChannel::class];
    }

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->greeting("Здравствуйте, {$this->data['name']}!")
            ->line('The introduction to the notification.')
            ->line('Thank you for using our application!');
    }
}
