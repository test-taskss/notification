<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

final class TelegramNotification extends Notification
{
    use Queueable;

    protected string|array $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function via($notifiable): array
    {
        return [TelegramChannel::class];
    }

    public function toTelegram($notifiable): TelegramMessage
    {
//        dd($notifiable);
        return TelegramMessage::create()
            ->to('-796535630') // $notifiable->telegram_chat_id
            ->content('Пользователь ' . $this->message . ' назначен автором.'); // $this->message
    }
}
