<x-app-layout>
{{--    <x-slot name="header">--}}
{{--        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">--}}
{{--            {{ __('Posts') }}--}}
{{--        </h2>--}}
{{--    </x-slot>--}}

    <div class="py-2">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-2 bg-dark">
                    <form method="POST" action="{{ route('posts.create') }}">
                        <x-success-message/>

                        @method('POST')
                        @csrf
                        <div class="flex justify-items-stretch">
                            <div class="flex justify-items-stretch w-full ml-1">
                                <x-text-input id="title" class="block mt-1 w-full" type="text" name="title"
                                              placeholder="Название"/>
                                <x-input-error :messages="$errors->get('title')" class="mt-2"/>
                            </div>
                            <div class="flex mt-1 ml-1 mr-1">
                                <x-primary-button>
                                    {{ __('Создать') }}
                                </x-primary-button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <table class="w-full border-hidden">
                    <thead class="bg-gray-700">
                        <tr class="text-left font-bold border-hidden">
                            <x-table-column>ID</x-table-column>
                            <x-table-column>Name</x-table-column>
                            <x-table-column>Email</x-table-column>
                            <x-table-column>Title</x-table-column>
{{--                            <x-table-column class="w-0"></x-table-column>--}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <x-table-column>{{ $post->id }}</x-table-column>
                                <x-table-column>{{ $post->user->name }}</x-table-column>
                                <x-table-column>{{ $post->user->email }}</x-table-column>
                                <x-table-column>{{ $post->title }}</x-table-column>
{{--                                <x-table-column>--}}
{{--                                    <div class="flex justify-center">--}}
{{--                                        <div>--}}
{{--                                            <div class="dropdown relative">--}}
{{--                                                <button--}}
{{--                                                    class="dropdown-toggle inline-block px-6 py-2.5 bg-purple-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-purple-700 hover:shadow-lg focus:bg-purple-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-purple-800 active:shadow-lg active:text-white transition duration-150 ease-in-out flex items-center whitespace-nowrap"--}}
{{--                                                    type="button"--}}
{{--                                                    id="dropdownMenuButton2"--}}
{{--                                                    data-bs-toggle="dropdown"--}}
{{--                                                    aria-expanded="false"--}}
{{--                                                >--}}
{{--                                                    Dropdown button--}}
{{--                                                    <svg--}}
{{--                                                        aria-hidden="true"--}}
{{--                                                        focusable="false"--}}
{{--                                                        data-prefix="fas"--}}
{{--                                                        data-icon="caret-down"--}}
{{--                                                        class="w-2 ml-2"--}}
{{--                                                        role="img"--}}
{{--                                                        xmlns="http://www.w3.org/2000/svg"--}}
{{--                                                        viewBox="0 0 320 512"--}}
{{--                                                    >--}}
{{--                                                        <path--}}
{{--                                                            fill="currentColor"--}}
{{--                                                            d="M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z"--}}
{{--                                                        ></path>--}}
{{--                                                    </svg>--}}
{{--                                                </button>--}}
{{--                                                <ul--}}
{{--                                                    class="dropdown-menu min-w-max absolute hidden bg-white text-base z-50 float-left py-2 list-none text-left rounded-lg shadow-lg mt-1 hidden m-0 bg-clip-padding border-none bg-gray-800"--}}
{{--                                                    aria-labelledby="dropdownMenuButton2"--}}
{{--                                                >--}}
{{--                                                    <h6--}}
{{--                                                        class="text-gray-400 font-semibold text-sm py-2 px-4 block w-full whitespace-nowrap bg-transparent"--}}
{{--                                                    >--}}
{{--                                                        Dropdown header--}}
{{--                                                    </h6>--}}
{{--                                                    <span--}}
{{--                                                        class="text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-300"--}}
{{--                                                    >Dropdown item text</span--}}
{{--                                                    >--}}
{{--                                                    <li>--}}
{{--                                                        <a--}}
{{--                                                            class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-300 hover:bg-gray-700 hover:text-white focus:text-white focus:bg-gray-700 active:bg-blue-600"--}}
{{--                                                            href="#"--}}
{{--                                                        >Action</a--}}
{{--                                                        >--}}
{{--                                                    </li>--}}
{{--                                                    <li>--}}
{{--                                                        <a--}}
{{--                                                            class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-300 hover:bg-gray-700 hover:text-white focus:text-white focus:bg-gray-700"--}}
{{--                                                            href="#"--}}
{{--                                                        >Another action</a--}}
{{--                                                        >--}}
{{--                                                    </li>--}}
{{--                                                    <li>--}}
{{--                                                        <a--}}
{{--                                                            class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-300 hover:bg-gray-700 hover:text-white focus:text-white focus:bg-gray-700"--}}
{{--                                                            href="#"--}}
{{--                                                        >Something else here</a--}}
{{--                                                        >--}}
{{--                                                    </li>--}}
{{--                                                    <li><hr class="h-0 my-2 border border-solid border-t-0 border-gray-300 opacity-25" /></li>--}}
{{--                                                    <li>--}}
{{--                                                        <a--}}
{{--                                                            class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-300 hover:bg-gray-700 hover:text-white focus:text-white focus:bg-gray-700"--}}
{{--                                                            href="#"--}}
{{--                                                        >Separated link</a--}}
{{--                                                        >--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div>--}}
{{--                                        <div class="w-0">--}}
{{--                                            <button id="option-button"--}}
{{--                                                    type="button"--}}
{{--                                                    data-dropdown-toggle="dropdown"--}}
{{--                                                    class="inline-flex justify-center rounded-md border--}}
{{--                                                    border-gray-700 bg-gray-700--}}
{{--                                                    hover:bg-gray-600 hover:border-gray-700--}}
{{--                                                    px-4 py-2--}}
{{--                                                    text-indigo-50--}}
{{--                                                    focus:outline-none focus:ring-2--}}
{{--                                                    transition--}}
{{--                                                    duration-150--}}
{{--                                                    ease-in-out"--}}
{{--                                                    aria-expanded="true"--}}
{{--                                                    aria-haspopup="true">--}}
{{--                                                ...--}}
{{--                                            </button>--}}
{{--                                        </div>--}}
{{--                                        <div class="absolute right-16 z-10 mt-1 w-36 origin-top-right--}}
{{--                                                rounded-md bg-gray-700 text-indigo-50 focus:outline-none--}}
{{--                                                transition-colors duration-300"--}}
{{--                                             role="menu"--}}
{{--                                             aria-orientation="vertical"--}}
{{--                                             aria-labelledby="option-button"--}}
{{--                                             tabindex="-1">--}}
{{--                                            <div role="none">--}}
{{--                                                <!-- Active: "bg-gray-100 text-gray-900", Not Active: "text-gray-700" -->--}}
{{--                                                <a href="#" class="text-indigo-50 block px-4 py-2 text-sm hover:bg-gray-600 focus:outline-none rounded-t-md" role="menuitem" tabindex="-1" id="menu-item-0">Edit</a>--}}
{{--                                                <a href="#" class="text-indigo-50 block px-4 py-2 text-sm hover:bg-gray-600 focus:outline-none rounded-b-md" role="menuitem" tabindex="-1" id="menu-item-1">Delete</a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                </x-table-column>--}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="py-2 max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="dark:bg-gray-800 shadow-sm sm:rounded-lg sm:py-2 px-6 lg:px-8">
            {!! $posts->withQueryString()->links() !!}
        </div>
    </div>
</x-app-layout>
